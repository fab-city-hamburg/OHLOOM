/*File Info--------------------------------------------------------------------
File Name: WarpClothBeam_Wood_NoCNC.scad
Project Name: OpenHardware LOOM - OHLOOM
License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name: Jens Meisner
Date: 08/01/20
Desc:  This file is part of the OHLOOM Project. Original design by Oliver Slueter, who made all wooden parts without a CNC Router. https://wiki.opensourceecology.de/Open_Hardware-Webstuhl_%E2%80%93_OHLOOM
Usage: 
./OHLoom_Documentation/Assembly_Guide/AssemblyGuide.md
./OHLoom_Documentation/User_Guide/OHLOOM_UserGuide.md
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any fur enter any further modification here
//--------------------------------------------------------------------------------


length=710;
diameter=35;
octo_d=45;
dist1=50;
dist2=206;
hole1=3;

difference()
{
    rotate([0,90,0])
    cylinder(h=length,d=diameter);
    translate([80+dist1,0,0])
    cylinder(h=octo_d,d=hole1,center=true);
    translate([80+dist2,0,0])
    cylinder(h=octo_d,d=hole1,center=true);
    translate([length-60-dist2,0,0])
    cylinder(h=octo_d,d=hole1,center=true);
    translate([length-60-dist1,0,0])
    cylinder(h=octo_d,d=hole1,center=true);
}
